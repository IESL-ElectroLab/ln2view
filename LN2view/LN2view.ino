/*
    LN2-View (El-En-Two-View)

    Copyright (C) 2015  Jann Eike KRUSE


    Monitors an analog input and displays the history as web server.
    
    
    LICENCE: APGL3+
    ===============
    
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#define PROGRAM_NAME "LN2-View"
#define VERSION_STRING "v2.0"

// At which level shoul the compressor stop operating and at which level start again:
#define OFF_LEVEL 90
#define ON_LEVEL  80

#define SocketBufferLength 128

bool setToOperate = false; // Set LN2 compressor to operate or not?
bool setToAuto    = true;  // Set LN2 compressor to switch on/off automatically? (DEFAULT: AUTO on)

// If we want our own favicon, the we need to define it 
// before we include the webduino library.
#include "favicon.h"

#include "SPI.h"
#include "Ethernet.h"
#include "WebServer.h"

#include <Watchdog.h>
Watchdog watchdog;

// Optionally, set the prefix of the web page to request. 
// (eg. https://www.server.com/prefix/index.html )
#define PREFIX ""

// Define here the title of the web page, 
// usually displayed in the title bar of the browser.
#define HTML_TITLE "LN2 view"

// This is the Arduino analog input pin where the 
// liquid nitrogen level meter is connected:
const int levelPin = A3;

// This is the Arduino input pin where the 
// liquid nitrogen on/off indicator is connected:
const int operatePin = A5;

// This is the Arduino input pin where the 
// liquid nitrogen on/off relay switch is connected:
const int relayPin = A1;

// This is the Arduino input pin where the 
// indicator LED is connected:
const int heartBeatPin = 3;
uint8_t heartBeatPhase = 0; // To cycle through brightness

// Keep an exponential time average of the reading from the level meter.
float average = 0.0;

// This is the array that holds LN2 level data.
// Accuracy is 0.5%, values of 201 and above are invalid.
const uint8_t dataStorageSize = 24*10;    // (Max 255!! I.e. about ten days, if logging every hour)
uint8_t historyLevel[dataStorageSize];
bool    historyRun[dataStorageSize];
uint8_t arrayPointer; // Points to the last written data (FIFO).

// Remember when was the last time we stored a reading.
unsigned long millisOfLastRead;

// How frequently do we want to update the reading?
const uint32_t recordPeriod = (uint32_t)60*60*1000;    // one hour (in milliseconds)

/* CHANGE THIS TO YOUR OWN UNIQUE VALUE.  The MAC number should be
 * different from any other devices on your network or you'll have
 * problems receiving packets. */
static uint8_t myMAC[] = { 0xde, 0xb4, 0x94, 0xf9, 0x27, 0x20 };


// LN2 compressor room network:
/* IP:      147.52.168.4 /24 (nitrogen.edu.biology.uoc.gr) 
   Gateway: 147.52.168.92
   DNS1:    147.52.168.1
   DNS2:    147.52.170.250 */
/**/
const byte   myIP[] = { 147,  52, 168,  4 };
const byte  myDNS[] = { 147,  52, 168,  1 };
const byte myGate[] = { 147,  52, 168, 92 };
const byte myMask[] = { 255, 255, 255,  0 };
/**/


// Electronics Lab network:
/*
const byte   myIP[] = { 139,  91, 170, 238 };
const byte  myDNS[] = { 139,  91, 197,  15 };
const byte myGate[] = { 139,  91, 170,  80 };
const byte myMask[] = { 255, 255, 255,   0 };
/**/

// "Shared with others" by GNOME network manager.
/*
const byte   myIP[] = {  10,  42,   0, 198 };
const byte  myDNS[] = {  10,  42,   0,   1 };
const byte myGate[] = {  10,  42,   0,   1 };
const byte myMask[] = { 255, 255, 255,   0 };
/**/

/* This creates an instance of the webserver.  By specifying a prefix
 * of "", all pages will be at the root of the server. */
WebServer webserver(PREFIX, 80);

/* This buffer is used to fill with data coming from the HTTP client.*/
char socketBuff[SocketBufferLength];
int socketBuffLen = SocketBufferLength;

/* commands are functions that get called by the webserver framework
 * they can read any posted data from client, and they output to the
 * server to send data back to the web browser. */
void plotCmd(WebServer &server, WebServer::ConnectionType type, char *, bool)
{
  /* this line sends the standard "we're all OK" headers back to the
     browser */
  server.httpSuccess();

  /* For a HEAD request, we just stop here after outputting the above headers.
     Otherwise, if we're handling a GET or POST, we'll output our data. */
  if (type != WebServer::HEAD)
  {
    /* this pre-defines some HTML text in read-only memory aka PROGMEM.
     * This is needed to avoid having the string copied to our limited
     * amount of RAM. */
    P(htmlHead) =
      "<html>\n"
      "  <head>\n"
      "    <title>" HTML_TITLE "</title>\n"
      "    <meta http-equiv=\"refresh\" content=\"60\">\n"
      "    <style type=\"text/css\">\n"
      "      body { font-family: sans-serif; min-width: 350px}\n"
      "      table.row { width:100%; text-align: right; font-size: 8pt; border:0px; border-collapse:collapse; margin:0px; padding: 0px;}\n"
      "      .hour     { padding: 0px; margin:0px; width:3em; background:#e0e0e0 }\n"
      "      .percent  { padding: 1px; margin:0px; width:6em; background:#e0e0e0 }\n"
      "      .bar      { padding: 0px; margin:0px }\n"
      "      div.rule   { clear:both; width:100%; height:1px; margin:0px; background:#eeeeee}\n"
      "      div.rule24 { clear:both; width:100%; height:1px; margin:0px; background:#999999}\n"
      "      p { font-size: 10pt; }\n"
      "    </style>\n"
      "  </head>\n"
      "  <body>\n"
      "    <a style=\'text-decoration:none\'  href=\'/api1/ctrl'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a>"
      "    <hr>";
/**/

    P(htmlTail) =
        "  <hr><small>Copyright (c) 2020 Jann Eike Kruse (jkruse@iesl.forth.gr) - <a href=\'https://gitlab.com/IESL-ElectroLab/ln2view\'>This software </a>is licensed under the AGPL v3.0</body>\n"
      "</html>\n";
      
    P(comprOn)       ="       <p style=\"float: right; background-color: LimeGreen; padding: 5px;\">RUNNING</p>";
    P(comprOff)      ="       <p style=\"float: right; background-color: OrangeRed; padding: 5px;\">STOPPED</p>";
    P(autoOn)        ="       <p style=\"float: right; background-color: yellow; padding: 5px;\">auto</p>";
    P(autoOff)       ="       <p style=\"float: right; background-color: note; padding: 5px;\">manual</p>";
    P(relayOn)       ="       <p style=\"float: right; background-color: LimeGreen; padding: 5px;\">on</p>";
    P(relayOff)      ="       <p style=\"float: right; background-color: OrangeRed; padding: 5px;\">off</p>";

    
    P(Row)      ="    <table class=\"row\">\n"
                 "      <tbody>\n"
                 "        <tr>\n";
    P(Hour)     ="          <td class=\"hour\"> ";
    P(Percent)  ="          <td class=\"percent\"> ";
    P(Bar1)     ="          <td class=\"bar\"><div class=\"bar\" style=\" width:";
    P(Bar2)     =                                           "%; background: rgb(";
    P(BarClose) =                                                               ")\"> &nbsp; </div></td>\n";
    P(Close)    =" </td>\n";
    P(rowClose) ="        </tr>\n"
                 "      </tbody>\n"
                 "    </table>\n";

    P(divRule)    ="    <div class=\"rule\"></div>\n";
    P(divRule24)  ="    <div class=\"rule24\"></div>\n";

    server.printP(htmlHead);

    if (digitalRead(operatePin) == LOW) 
      server.printP(comprOff);
    else
      server.printP(comprOn);

    if (setToOperate) 
      server.printP(relayOn);
    else
      server.printP(relayOff);

    if (setToAuto) 
      server.printP(autoOn);
    else
      server.printP(autoOff);

    server.print("    <H1>Current Level: ");
    server.print(levelFromAnalog(average)/2.0);
    server.print("%</H1>\n");
    server.print("<H2>History:</H2>\n");
    
    server.printP(Row);    
    server.printP(Hour);  
    server.print("Hours ago");  
    server.printP(Close);  
    server.printP(Percent); 
    server.print("Tank level");    
    server.printP(Close);  
    server.printP(Bar1);    
    server.print(100);    
    server.printP(Bar2);    
    server.print("(200,200,200)");
    server.printP(BarClose);    
    server.printP(rowClose);   

    server.printP(divRule);
    
    for (unsigned int data,index = 0; index < dataStorageSize; index++) {
      watchdog.reset();
      data = historyLevel[(dataStorageSize+arrayPointer-index)%dataStorageSize];
      if (data != 255) {
        server.printP(Row);     
        server.printP(Hour);   
        if (historyRun[(dataStorageSize+arrayPointer-index)%dataStorageSize])
          server.print("* ");
        else
          server.print(" ");
        server.print(index+1);    
        server.printP(Close);   
        server.printP(Percent); 
        server.print(data/2.0);    
        server.print("%");        
        server.printP(Close);  
        server.printP(Bar1);   
        server.print(data/2);   
        server.printP(Bar2);    
        server.print((200-data)*(200-data)/200);
        server.print(",");
        server.print(data*data/200);
        server.print(",");
        server.print(int(((200.0-data)*data/2600.0)*((200.0-data)*data/2600.0)*((200.0-data)*data/2600.0)*((200.0-data)*data/2600.0)));
        server.printP(BarClose);    
        server.printP(rowClose);  
        if ( (index%24) == 23) server.printP(divRule24);
          else server.printP(divRule);
      } // end if
    } // end for
    server.print("<hr><small>Uptime: ");
    uint16_t seconds = millis()/1000;
    if ( (seconds/60/60) < 10 ) server.print("0");
    server.print(seconds/60/60);
    server.print(":");
    if ( (seconds/60)%60 < 10 ) server.print("0");
    server.print((seconds/60)%60);
    server.print(":");
    if ( (seconds)%60 < 10 ) server.print("0");
    server.print((seconds)%60);
    server.printP(htmlTail);

  }
}

void ctrlCmd(WebServer &server, WebServer::ConnectionType type, char *, bool) {
  setToAuto = false;
  setToOperate = false;
  /* this line sends the standard "we're all OK" headers back to the
     browser */
  server.httpSuccess();

  /* For a HEAD request, we just stop here after outputting the above headers.
     Otherwise, if we're handling a GET or POST, we'll output our data. */
  if (type != WebServer::HEAD)
  {
    P(htmlRedirect) =
      "<html>\n"
      "  <head>\n"
      "    <title>" HTML_TITLE "</title>\n"
      "    <meta http-equiv=\"refresh\" content=\"10; url=/index.html\">\n"
      "  </head>\n"
      "  <body>\n"
//      "    <form action=\"/api1/on\" method=\"post\">"
//      "      <input type=\"submit\" value=\"ON\">"
//      "    </form>"
      "    <form action=\"/api1/off\" method=\"post\">"
      "      <input type=\"submit\" value=\"OFF\">"
      "    </form>"
      "    <form action=\"/api1/auto\" method=\"post\">"
      "      <input type=\"submit\" value=\"AUTO\">"
      "    </form>"
      "  </body>\n";
    server.printP(htmlRedirect);
  }
}

void offCmd(WebServer &server, WebServer::ConnectionType type, char *, bool) {
  setToAuto = false;
  setToOperate = false;
  /* this line sends the standard "we're all OK" headers back to the
     browser */
  server.httpSuccess();

  /* For a HEAD request, we just stop here after outputting the above headers.
     Otherwise, if we're handling a GET or POST, we'll output our data. */
  if (type != WebServer::HEAD)
  {
    P(htmlRedirect) =
      "<html>\n"
      "  <head>\n"
      "    <title>" HTML_TITLE "</title>\n"
      "    <meta http-equiv=\"refresh\" content=\"0; url=/index.html\">\n"
      "  </head>\n"
      "  <body>\n"
      "   off\n"
      "  </body>\n";
    server.printP(htmlRedirect);
  }
}


void onCmd(WebServer &server, WebServer::ConnectionType type, char *, bool) {
  setToAuto = false;
  setToOperate = true;
  /* this line sends the standard "we're all OK" headers back to the
     browser */
  server.httpSuccess();

  /* For a HEAD request, we just stop here after outputting the above headers.
     Otherwise, if we're handling a GET or POST, we'll output our data. */
  if (type != WebServer::HEAD)
  {
    P(htmlRedirect) =
      "<html>\n"
      "  <head>\n"
      "    <title>" HTML_TITLE "</title>\n"
      "    <meta http-equiv=\"refresh\" content=\"0; url=/index.html\">\n"
      "  </head>\n"
      "  <body>\n"
      "   on"
      "  </body>\n";
    server.printP(htmlRedirect);
  }
}


void autoCmd(WebServer &server, WebServer::ConnectionType type, char *, bool) {
  setToAuto = true;
  if ( (setToAuto) && (levelFromAnalog(average)/2.0 <  OFF_LEVEL) ) setToOperate = true;  // Start even if we're just below the higer (OFF) level.
  /* this line sends the standard "we're all OK" headers back to the
     browser */
  server.httpSuccess();

  /* For a HEAD request, we just stop here after outputting the above headers.
     Otherwise, if we're handling a GET or POST, we'll output our data. */
  if (type != WebServer::HEAD)
  {
    P(htmlRedirect) =
      "<html>\n"
      "  <head>\n"
      "    <title>" HTML_TITLE "</title>\n"
      "    <meta http-equiv=\"refresh\" content=\"0; url=/index.html\">\n"
      "  </head>\n"
      "  <body>\n"
      "   *** AUTO ***"
      "  </body>\n";
    server.printP(htmlRedirect);
  }
}


float exponential_average(float oldAverage, float newValue)
  {
    return (oldAverage*255)/256 + newValue/256;
  };


// Analog readings are from 0 to 1023. 
// This need to be converted to values from 1 to 200 stored internally.
// Since values are rounded down when casting into 8 bit, 
// we multiply with 200.49, so that 1022 converts to 200.
uint8_t levelFromAnalog(float value)
{
  return (value/1023.0)*200.49;
}

void setup()
{
  pinMode(heartBeatPin, OUTPUT); 

  digitalWrite(relayPin,LOW);
  pinMode(relayPin, OUTPUT); 

  pinMode(operatePin, INPUT_PULLUP); 

  /* Initialize the data array */
  for (arrayPointer = 0; arrayPointer < dataStorageSize; arrayPointer++) {
    historyLevel[arrayPointer] = 255;
  }

  /* Stabilize the average */
  for (int i = 0; i < 1000; i++) {
    average = exponential_average(average, analogRead(levelPin));
    delay(1);
  }

  millisOfLastRead = millis();
  arrayPointer = 0;
/*
  Serial.begin(9600);
  Serial.print(F("Starting \""));
  Serial.print(PROGRAM_NAME);
  Serial.print(F("\" version "));
  Serial.print(VERSION_STRING);
  Serial.print(F(".\n"));
*/
  /* Initialize the Ethernet adapter: 
   *  Use the version with IP parameters if known,
   *  otherwise use the version with MAC only with DHCP.*/

  Ethernet.begin(myMAC, myIP, myDNS, myGate, myMask);
//  Ethernet.begin(myMAC);
/*
  Serial.print("  IP: ");
  Serial.println(Ethernet.localIP());
  Serial.print("MASK: ");
  Serial.println(Ethernet.subnetMask());
  Serial.print(" DNS: ");
  Serial.println(Ethernet.dnsServerIP());
  Serial.print("GATE: ");
  Serial.println(Ethernet.gatewayIP());
*/
  /* setup our default command that will be run when the user accesses
   * the root page on the server */
  webserver.setDefaultCommand(&plotCmd);

  /* run the same command if you try to load /index.html, a common
   * default page name */
  webserver.addCommand("index.html", &plotCmd);

  /* serve the CTRL page (with the buttons) */
  webserver.addCommand("api1/ctrl", &ctrlCmd);

  /* run the ON command */
  webserver.addCommand("api1/on", &onCmd);

  /* run the OFF command */
  webserver.addCommand("api1/off", &offCmd);

  /* run the AUTO command */
  webserver.addCommand("api1/auto", &autoCmd);

  /* start the webserver */
  webserver.begin();

  watchdog.enable(Watchdog::TIMEOUT_1S);


}

void loop()
{
  watchdog.reset();
  average = exponential_average(average, analogRead(levelPin));
  
  /* process incoming connections one at a time forever */
  socketBuffLen = SocketBufferLength;
  webserver.processConnection(socketBuff, &socketBuffLen);

  // Record a new value every recordPeriod:
  if ( (millis() - millisOfLastRead) >= (recordPeriod) ) {
    millisOfLastRead = millis();
    arrayPointer = (arrayPointer + 1) % dataStorageSize; // address next index
    historyLevel[arrayPointer] = levelFromAnalog(average);
    historyRun[arrayPointer] = digitalRead(operatePin);
  }

  analogWrite(heartBeatPin, heartBeatPhase++);
  delay(1); //rate limiting

  if ( (setToAuto) && (levelFromAnalog(average)/2.0 > OFF_LEVEL) ) setToOperate = false;
  if ( (setToAuto) && (levelFromAnalog(average)/2.0 <  ON_LEVEL) ) setToOperate = true;
  
  if (setToOperate == true) {
    //Serial.println(levelFromAnalog(average)/2.0);
    digitalWrite(relayPin, HIGH);
  } else {
    digitalWrite(relayPin, LOW);    
  }

}
