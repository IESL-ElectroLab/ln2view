# Liquid Nitroget Monitor

Arduino-based remote sensing and remote control node with web server to read the level of the liquid nitrogen tank, and also to read and set the operation of the liquid nitrogen producing compressor.